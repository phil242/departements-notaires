<?php

global $connect;

//Liste déroulante des nom d'individu pour la liste déroulante
if ( isGestionnaire() || isAdmin() ){
	$res = $connect->query("select distinct * from (SELECT distinct nom_usage as nom from log_recherche Union select
        distinct nom_civil as nom from log_recherche) as log order by nom ASC;");
}
else{
	$res = $connect->query("select distinct * from (SELECT distinct nom_usage as nom from log_recherche where nom_etude='".getSession('loginNotaire')."' union select
        distinct nom_civil as nom from log_recherche where nom_etude='".getSession('loginNotaire')."') as log order by nom ASC");
}

$liste_deroulante_individu=array();
while ($var = $res->fetch())
{
	$liste_deroulante_individu[] = $var['nom'];

}

//Liste déroulante des nom d'étude pour la liste déroulante
$liste_deroulante_etude=array();
$res = $connect->query("SELECT distinct nom_etude,libelle from log_recherche order by libelle ASC;");
        
while ($var = $res->fetch())
{
	$liste_deroulante_etude[$var['nom_etude']]= $var['libelle'];
}

//liste déroulante de l'année
$res = $connect->query("SELECT distinct year(date) as annee from log_recherche;");
$annee_listing=array();
while ($var = $res->fetch())
{
	$annee_listing[] = $var['annee'];
	
}


/**
 * Exécute la recherche et renvoie un tableau de résultat.
 * 
 * @param array $criteres contient les clés/valeurs pour la recherche
 *    Clés obligatoires : 'day_start', 'month_start', 'year_start', 'day_end', 'month_end', 'year_end'
 *    Clés optionnelles : 'selecto', 'type_reponse', 'nom'
 *
 * @return String chaîne qui contient la tableau du résultat trouvé
 */
function notairesListing($criteres)
{
	global $connect;
	
	$message = "";
	// Paramètres obligatoires
	$selecto= getValue($criteres, 'selecto');
	$type_reponse= getValue($criteres, 'type_reponse');
	$nom= getValue($criteres, 'nom');
	$year_end= getValue($criteres, 'year_end');
	$month_end= getValue($criteres, 'month_end');
	$day_end= getValue($criteres, 'day_end');
	$year_start= getValue($criteres, 'year_start');
	$month_start= getValue($criteres, 'month_start');
	$day_start= getValue($criteres, 'day_start');
	
	if ( isGestionnaire() || isAdmin() ){
		$not="";
	}
	else{
		$not=" and nom_etude='".getSession('loginNotaire')."' ";
	}
	
	$date_superieurNotaire = $year_end.'-'. $month_end.'-'.$day_end.' 23:59:00';
	$date_inferieurNotaire = $year_start.'-'.$month_start.'-'.$day_start.' 00:00:00';
	
	$sql_selecto="";
	if($selecto!=""){
		$sql_selecto=" and nom_etude='".$selecto."'";
	}
	
	$sql_type_reponse="";
	if($type_reponse!=0){
		$sql_type_reponse=" and reponse='".$type_reponse."'";
	}
	
	$sql_nom="";
	if($nom !=""){
		$sql_nom=" and (nom_usage='".$nom."' OR nom_civil='".$nom."')";
	}
	
	$sql = "SELECT * from log_recherche where (date BETWEEN '".$date_inferieurNotaire."' AND '".$date_superieurNotaire."' 
	$sql_selecto $sql_type_reponse $sql_nom) $not  order by date desc ;";
	
	$res = $connect->query($sql);
	
	// TODO : utiliser un template pour générer le tableau

	// Affichage des log de recherches
	$message.= '<table id="tablesorter" class="table_admin" width=1260 style="margin-left:-125px;margin-bottom:40px;">';
	$message.= '<thead><tr>';
	if ( isGestionnaire() || isAdmin() ){$message.= '<th>N&ordm; recherche</th><th>Etude</th>';}
	$message.= '<th>Nom</th><th>Prénom</th><th>Date naissance</th><th>Date décès</th><th>Lieu décès</th><th>Date acte</th>';
	if ( isGestionnaire() || isAdmin() ){$message.= '<th>N&ordm; individu</th>';}
	$message.= '<th>Réponse</th><th>Lien PDF</th><th width=138>Date recherche</th></tr></thead>';

	while ($row = $res->fetch())
	{
		$message.= '<tr class=tabliste>';
		if ( isGestionnaire() || isAdmin() ){
			$message.= '<td class=tabliste>'.$row['id_recherche'].'</td>';
			$message.= '<td class=tabliste>'.$row['libelle'].'</td>';
		}
		
		$message.= '<td class=tabliste>';
		if($row['nom_civil']!=null){$message.= 'Nom d\'usage : ';}
		$message.= $row['nom_usage'];
		if($row['nom_civil']!=null){$message.= '<br>Nom civil : '.$row['nom_civil'];}
		$message.= '</td>';
		$message.= '<td  class=tabliste>';
		if($row['prenom2']!=null){ $message.= 'Prénom : ';}
		$message.= $row['prenom'];
		$iii=2;
		if($row['prenom2']!=null){
			if(mb_substr_count($row['prenom2'],"####")>0){
				$pp=explode("####",$row['prenom2']);
				foreach($pp as $ff){
					$message.= '<br>Prénom '.$iii.' : '.$ff;
					$iii++;
				}
			}else{
				$message.= '<br>Prénom '.$iii.' : '.$row['prenom2'];
			}
		}
		$message.= '</td>';
		list($annee_n, $mois_n, $jour_n) = preg_split('/[.\-\/]+/', $row['date_naissance']);
		$message.= '<td  class=tabliste>'.$jour_n.'/'.$mois_n.'/'.$annee_n.'</td>';
		list($annee_d, $mois_d, $jour_d) = preg_split('/[.\-\/]+/', $row['date_deces']);
		$message.= '<td  class=tabliste>'.$jour_d.'/'.$mois_d.'/'.$annee_d.'</td>';
		$message.= '<td  class=tabliste>'.stripslashes($row['lieu_deces']).'</td>';
		list($annee_a, $mois_a, $jour_a) = preg_split('/[.\-\/]+/', $row['date_acte']);
		$message.= '<td  class=tabliste>'.$jour_a.'/'.$mois_a.'/'.$annee_a.'</td>';
		if ( isGestionnaire() || isAdmin() ){
			$message.= '<td  class=tabliste>'.$row['id_individus'].'</td>';
		}
		$reponse = getResponseLabel ($row['reponse']);
		$message.= '<td  class=tabliste>'.$reponse.'</td>';
		if ($row['reponse']==3)
		{
			$message.= '<td  class=tabliste><a class=pdf href="gen_pdf.php?num_rech='.$row['id_recherche'].'" target="_blank">PDF</a></td>';
		}
		elseif ($row['reponse']==1)
		{
			$message.= '<td  class=tabliste><a class=pdf href="gen_pdf.php?num_rech='.$row['id_recherche'].'" target="_blank">PDF</a></td>';
		}
		elseif ($row['reponse']==2)
		{
			$message.= '<td  class=tabliste><a class=pdf href="gen_pdf.php?num_rech='.$row['id_recherche'].'" target="_blank">PDF</a></td>';
		}
		else
		{
			$message.= '<td  class=tabliste>'.$row['lien_pdf'].'</td>';
		}
		$message.= '<td  class=tabliste>'.$row['date'].'</td></tr>';
		
	}
	$message.= '</table>';
	
	return $message;
	
}




?>
