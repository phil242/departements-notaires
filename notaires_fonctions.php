﻿<?php
require_once 'config.php';
require_once 'vendor/autoload.php';

$jour = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
$mois = array("","Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");

// profilNotaire : 1=notaire 3=admin 4=gestionnaire
const PROFILE_NOTAIRE  = 1;
const PROFILE_ADMIN    = 3;
const PROFILE_GESTION  = 4;

$PROFILE_LABELS = [
  0 => "Indéterminé",
  1 => "Etude",
  3 => "Administrateur",
  4 => "Gestionnaire"
];

// Code des réponses possibles au notaire
const REPONSE_ERREUR         = 0;
const REPONSE_RECUPERATION   = 1;
const REPONSE_INDUS_PROBABLE = 2;
const REPONSE_INCONNU        = 3;
const REPONSE_AMBIGU         = 4;
const REPONSE_MIN            = REPONSE_RECUPERATION;
const REPONSE_MAX            = REPONSE_AMBIGU;
const REPONSE_INT_DEMANDE    = 10; // code interne pour informer le gestionnaire sur une demande déjà réalisée 

$REPONSE_LABELS = [
    0 => "Erreur",
    1 => "Récupération",
    2 => "Indus probable",
    3 => "Inconnu",
    4 => "Ambigu"
];

/**
 * A appeler explicitement au début de tout script nécessistant un contexte utilisateur valide.
 * Si la session n'est pas valide, renvoie à la page index.php
 */
function sessionCheck()
{
  $valid = false;
  sessionUse();
  if ( isLoggedIn() ) {
    // Vérifie inactivité
    // TODO rendre paramétrable délai inactivité
    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 2700)) {
      $valide = false; 
    } else {
      $_SESSION['LAST_ACTIVITY'] = time();
      $valid = true;
    }
  }
  if (!$valid) {
    ob_end_clean();
    sessionLogout();
    header('Location: index.php');
  }
}

/**
 * Démarre l'utilisation des variables de session. Si les données de session existaient, les charges 
 */
function sessionUse()
{
  if ( ! isset($_SESSION) ) {
    session_start();
  }
}
/**
 * Nettoie la session et les variables permettant d'identifier qu'un utilisateur est actif (cf. isLoggedIn)
 */
function sessionLogout()
{
  if ( isset($_SESSION) ) {
    $_SESSION['loginNotaire'] = null;
    $_SESSION['profileNotaire'] = null;
  }
  // Si vous voulez détruire complètement la session, effacez également
  // le cookie de session.
  // Note : cela détruira la session et pas seulement les données de session !
  if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
        );
  }
  if ( isset($_SESSION) ) {session_destroy();}
}

/**
 * 
 * @param $loginNotaire
 * @param $passNotaire
 * @return  Array [err_code, user_login, user_email]
 *  err_code peut valoir :"OK", "OK_FIRST", "TOO_MANY_ERROR", "BAD_LOGIN", "ERR_SYS"
 *  user_login et user_email sont renseignés si l'utilisateur existe dans le système
 */
function sessionLogin($loginNotaire, $passNotaire) 
{
  global $connect;
  
  $err_code   = "BAD_LOGIN";
  $user_login = "";
  $user_email = "";
  
  $max_echec = config("max_login");
  
  // on teste si une entrée de la base contient ce couple login / pass
  $sql = "SELECT id FROM membre WHERE login=". sql_escape($loginNotaire)." AND pass_md5=".sql_escape(md5($passNotaire)). " AND echec<$max_echec";
  $result = $connect->query($sql);
  $data = $result->fetch();
  
  // L'id est forcément unique
  if ( $data && array_key_exists('id', $data) ) {
      $id_user = $data['id'];
      
    // Initialise une nouvelle session
    sessionUse();
  
    // Remet à zéro les tentatives de connexion
    $requ = "UPDATE membre set echec=0 where login=".sql_escape($loginNotaire);
    $reset_echec = $connect->query($requ);
    
    // Fixe #43 Supprime le jeton éventuel de reinit du mot de passe au succès de la connexion
    $req_delete_reinit_mdp = "DELETE FROM reinit_mdp WHERE id_membre=$id_user";
    $connect->query($req_delete_reinit_mdp);

    // Charge les données de l'utilisateur
    $profile = $connect->query("SELECT * FROM membre WHERE id=$id_user");
    $var = $profile->fetch();
    
    $_SESSION['loginNotaire']     = $var['login'];
    $_SESSION['libelleNotaire']   = $var['libelle'];
    $_SESSION['id_membre']        = $var['id'];
    $_SESSION['mailNotaire']      = $var['adresse_mail'];
    $_SESSION['profileNotaire']   = $var['profile'];
    $_SESSION['profile_label']    = profileLabel();

    $first_login = ($var['premiere_connexion']==0);
    $date_login = date("Y-m-d H:i:s");
    
    log_membre($var ['id'], $var['login'], $var ['libelle'], $var ['adresse_mail'], $date_login);
    
    $err_code   = $first_login ? "OK_FIRST" : "OK";
    $user_login = $var['login'];
    $user_email = $var['adresse_mail'];
    
  }
  // si on ne trouve aucune réponse, le visiteur s'est trompé soit dans son login, soit dans son mot de passe
  else {
    
    // Vérifie si l'utilisateur existe pour mettre à jour le nombre d'échecs et la clé de déverrouillage
    $profile = $connect->query("SELECT * FROM membre WHERE login=" . sql_escape($loginNotaire));
    $var = $profile->fetch();
    if ($var) {
      $id_user    = $var['id']; 
      $user_login = $var['login'];
      $user_email = $var ['adresse_mail'];
      
      // Le compte existe, on vérifie le nombre d'échecs
      $nb_echec = $var['echec'];
      if ($nb_echec < $max_echec) {
        // Mise à jour nb échecs
        $nb_echec++;
        $connect->query("UPDATE membre set echec=". $nb_echec . " where id=" . $id_user);
        $err_code = "BAD_LOGIN";
      } else {
        $err_code = "TOO_MANY_ERROR";
      }
    } else {
      $err_code = "BAD_LOGIN";
    }
  }
  return [$err_code, $user_login, $user_email];
}

// Générer une chaine de caractère unique et aléatoire
function random($car)
{
  $string = "";
  $chaine = "abcdefghijklmnpqrstuvwxy";
  srand((double) microtime() * 1000000);
  for ($i = 0; $i < $car; $i ++) {
      $string .= $chaine[rand() % strlen($chaine)];
  }
  return $string;
}


function log_membre($id, $login, $libelle, $mail, $date)
{
  global $connect;
  
  $requ_insert = "INSERT INTO `log_membre` (`id`, `id_membre`, `login` , `libelle`, `adresse_mail`, `date`) VALUES (NULL, " . $id . ",'" . $login . "', '" . $libelle . "', '" . $mail . "', '" . $date . "')";
  $result = $connect->query($requ_insert);
}


/**
 * Selon le profile de l'utilisateur connecté, route vers la bonne page d'accueil.
 * A la première connexion suite à la création du compte, la page d'accueil permet de modifier le mot de passe.
 * 
 * @param int $profile
 * @param bool $first_login true si première connexion
 */
function getWelcomePage($profile, $first_login) 
{
  $ref = "";
  if ( $first_login ) $ref = "index.php?index=first";
  else {
    switch ($profile) {
      case PROFILE_ADMIN :
        $ref=  "index.php?index=admin_accueil";
        break;
      case PROFILE_GESTION :
        $ref=  "index.php?index=recherches";
        break;
      case PROFILE_NOTAIRE:
        $ref=  "index.php?notaire=recherche";
        break;
    }
  }
  return $ref;
}

/**
 * Selon le profil de l'utilisateur connecté, renvoie le nom du template de sa page d'accueil
 *
 */
function getWelcomeTemplate()
{
  $ret = "";
  switch (profileNotaire()) {
    case PROFILE_ADMIN :
      $ret=  "sys/admin_accueil.tpl";
      break;
    case PROFILE_GESTION :
      $ret=  "welcome.tpl";
      break;
    case PROFILE_NOTAIRE:
      $ret=  "sys/frm_recherche.tpl";
      break;
    default :
      $ret=  "welcome.tpl";
  }
  return $ret;
}

/**
 * Envoie un email pour dévérouiller le compte après plusieurs tentatives infructueuses
 * @param $user_login
 * @param $user_email
 * @return true si l'email a été envoyé
 */
function sendMailUnlock($user_login, $user_email)
{
  global $connect, $mail;
  
  $ret = false;
  
  // Pour éviter appel successif, vérifie au moins que le compte est bien verrouillé
  $profile = $connect->query("SELECT id FROM membre WHERE login=" . sql_escape($user_login) 
      . " and adresse_mail=" . sql_escape($user_email)
      . " and echec >=" . config("max_login") );
  $var = $profile->fetch();
  if ($var) {
    if ( $var['id'] > 0 ) {
      $unclock_key = mt_rand();
      
      // La clé de déverrouillage est stockée dans la session, donc n'est valable que si l'utilisateur ne ferme
      // pas sa fenêtre
      $_SESSION['unlock_key'] = $unclock_key ; 
      $_SESSION['loginNotaire'] = $user_login;
      
      $mail->AddAddress($user_email);
      $mail->Subject = "[" . config('nom_application') . "] - Déverrouillage du compte $user_login " . config('mess_prod');
      $mail->Body = templateRender("sys/mail_deverouille.tpl", ["unlock_key"=>$unclock_key]);
      
      if ( doSendMail() ) {
         $mail->Send();
      }
      $ret = true;
    }
  }
  return $ret;
}

/**
 * Envoie l'email de réinit du MDP si l'identifiant existe et que l'envoi n'a pas déjà été fait
 * @param $user_login
 * @return string message retour
 */
function sendMailReinitPass($user_login)
{
    
  // Les messages retours sont flous intentionnellement pour ne pas permettre de deviner si un identifiant existe ou non
  global $connect;
  
  $message = "";
  $sql = "SELECT id,login,adresse_mail from membre where login=" . sql_escape($user_login);
  $res = $connect->query($sql);
  $var = $res->fetch();
  if ( $var ) {
    $user_id = $var['id'];
    $user_email = $var['adresse_mail'];
    
    if ( ! existsReinitPass($user_id, $user_email) ) {
      $unlock_key = mt_rand();
      // Insertin en base d'abord, si cela rate, on n'envoie pas l'email
      $sql = 'INSERT INTO reinit_mdp (id_membre, adresse_mail,tkn_cd)  values (' . $user_id . ',' . 
        sql_escape($user_email) . ',' . $unlock_key . ')';
      if ($connect->query($sql)) {
        // Envoie email
        // TODO gérer erreur envoi email
        _sendMailReinitPass($user_id, $user_login, $user_email, $unlock_key);
        $message = "Si votre identifiant existe, un message électronique unique est envoyé sur l'adresse de messagerie correspondant.";
      }
      else {
        $message = "Une erreur est survenue, veuillez réessayer.";
      }
    } else {
      $message = "Si votre identifiant existe, un message électronique unique est envoyé sur l'adresse de messagerie correspondant.";
    }
  }
  else
  {
    $message = "Si votre identifiant existe, un message électronique unique est envoyé sur l'adresse de messagerie correspondant.";
  }
  return $message;
}

/**
 * Vérifie si le token de réinit stocké en passe pour cet utilisateur est cleui fourni
 * @param $user_id
 * @param $token_given
 * @return boolean true si la vérification
 */
function checkReinitPassToken($user_id, $token_given) 
{
  global $connect;
  
  $ret = false;
  
  $sql = "SELECT tkn_cd from reinit_mdp where id_membre=" . sql_escape($user_id);
  
  $res = $connect->query($sql);
  $var = $res->fetch();
  $token = $var['tkn_cd'];
  $ret = ($token_given == $token);
  return $ret;
}

/**
 * Modifie le mot de passe
 * @return array(ok, msg). ok est true si la modification a été faite sans erreur
 */
function passReinit($user_id, $pass, $pass_confirm)
{
  global $connect;
  $ok = false;

  // Vérifier les paramètres et les règles d'abord
  list ($err, $err_mess) = passCheckNew($user_id, $pass, $pass_confirm, true/*cgu_accept*/);
  
  if ($err=="OK") {  
    $result = _passUpdate($pass, $user_id);
    if ($result) {
      $req_delete = "DELETE FROM reinit_mdp WHERE id_membre=$user_id";
      logDebug("req_delete : " . $req_delete, 2);
      if ($connect->query($req_delete) ) {
        $ok = true;
      }
    }
    if (!$ok) { // echec SQL
      $err_mess = "Erreur lors de la mise à jour du mot de passe dans la base de données";
    }
  }
  return [$ok, $err_mess];
}

/**
 * Modifie le mot de passe de l'utilisateur connecté
 * @param $pass
 * @param $pass_confirm
 * @param $cgu_accept si non true, erreur
 * @param $pass_old si spécifié, on vérifie d'abord que l'ancien mot de passe est le bon
 * @return array(ok, msg). ok est true si la modification a été faite sans erreur
 */
function passChange($pass, $pass_confirm, $cgu_accept, $pass_old=null)
{
  global $connect;
  $go_on = true;
  $ok = false;
  $err_mess = "";
  
  $user_id = getSession("id_membre");
  
  if (!empty($pass_old) ) {
    if (_passCrypt($pass_old) != _getUserPass()) {
      $err_mess = "L'ancien mot de passe ne correspond pas à celui de l'utilisateur.";
      $go_on = false;
    } 
  }
  if ($go_on) {
    // Vérifier les paramètres et les règles d'abord
    list ($err, $err_mess) = passCheckNew($user_id, $pass, $pass_confirm, $cgu_accept);
  
    if ($err=="OK") {
      $result = _passUpdate($pass, $user_id);
      if ($result) {
        $ok = true;
      } else { // echec SQL
        $err_mess = "Erreur lors de la mise à jour du mot de passe dans la base de données";
      }
    }
  }
  return [$ok, $err_mess];
}

/**
 * Débloque le compte utilisateur
 * @return true si succès
 */
function unlockAccount($given_key) 
{
  global $connect;
  $ret = false;
  
  if (!empty($given_key) && ($given_key==getSession('unlock_key')) ) {
    $req = "update membre set echec=0 where login=" . sql_escape(getSession('loginNotaire'));
    $update = $connect->query($req);
    sessionLogout();
    
    $ret = ($update != FALSE);
  }
  return $ret;
}

/**
 * 
 * @param  $id_membre
 * @param  $adresse_mail
 * @return true si un code de réinitialisation du mot de passe a déjà été envoyé par email
 */
function existsReinitPass($id_membre, $adresse_mail)
{
  global $connect;
  
  $sql_exist = "SELECT count(*) existe FROM reinit_mdp where id_membre=" . $id_membre . " and adresse_mail=" . sql_escape($adresse_mail);
  
  logDebug("sql_exist : $sql_exist");
  
  $verif_exist = $connect->query($sql_exist);
  $verif_result = $verif_exist->fetch();
  return ($verif_result['existe']>=1);
}


/**
 * Envoie l'email de réinitialisation du mot de passe
 * @param int $user_id
 * @param string $user_login
 * @param string $user_email
 * @param int $unlock_key 
 * @return number le code de déverrouillage
 */
function _sendMailReinitPass($user_id, $user_login, $user_email, $unlock_key)
{
    global $mail;

    $url_reinit = config("adresse_url_appli")."/index.php?index=reinit&token=$unlock_key&id=$user_id";
    
    $mail->AddAddress($user_email);
    $mail->Subject = "[" . config("nom_application") . "] - Réinitialisation de votre mot de passe" . config("mess_prod");
    $mail->Body = templateRender("sys/mail_reinit_pass.tpl", ["url_reinit"=>$url_reinit]); 
    
    // Envoi réel en prod seulement
    if ( doSendMail() ) {
        $mail->Send();
    }
    return $unlock_key;
}


/**
 * Traduit de façon intelligible la réponse à la recherche du notaire.
 * @param int $reponse_code
 * @return string le libellé de la réponse
 */
function getResponseLabel($reponse_code)
{
  global $REPONSE_LABELS;
  if ($reponse_code >= REPONSE_MIN && $reponse_code<= REPONSE_MAX) {
    return $REPONSE_LABELS[$reponse_code];
  }
  return $REPONSE_LABELS[REPONSE_ERREUR];
}


/**
 * 
 * @return int un numéro aléatoire unique identifiant les requêtes associés à une recherche utilisateur 
 */
function newSearchId()
{
  global $connect;
  
  $aleatoire = mt_rand();
  
  $sql = "SELECT count(*) as reponse from log_recherche where id_recherche = " . $aleatoire . ";";
  $res = $connect->query($sql);
  $var = $res->fetch();
  // si on obtient une réponse, le numéro est déjà utilisé
  if ($var['reponse'] == 0) {
      return $aleatoire;
  } else {
    // Sinon on cherche un autre numéro libre
      return newSearchId();
  }
}


/**
 *  Supprime les accents
 */
function removeAccent($str, $encoding = 'utf-8')
{
    // transformer les caractères accentués en entités HTML
    $str = htmlentities($str, ENT_NOQUOTES, $encoding);
    
    // remplacer les entités HTML pour avoir juste le premier caractères non accentués
    // Exemple : "&ecute;" => "e", "&Ecute;" => "E", "à " => "a" ...
    $str = preg_replace('#&([A-za-z])(?:acute|grave|cedil|circ|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    
    // Remplacer les ligatures tel que : é, é...
    // Exemple "œ" => "oe"
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str);
    // Supprimer tout le reste
    $str = preg_replace('#&[^;]+;#', '', $str);
    
    return $str;
}

// Fonction d'affichage du tableau des statistique de recherches
function staque($mois, $annee)
{
    global $connect;
    
    $sql = "SELECT count(*) as question FROM log_recherche where Month(date)=" . $mois . " and Year(date)=" . $annee . ";";
    $res = $connect->query($sql);
    $var = $res->fetch();
    return $var['question'];
}

function stats_rep($mois, $annee)
{
    global $connect;
    
    $sql = "SELECT count(reponse) as reponse FROM log_recherche where Month(date)=" . $mois . " and Year(date)=" . $annee . ";";
    $res = $connect->query($sql);
    $var = $res->fetch();
    return $var['reponse'];
}

function starepnat($mois, $type, $annee)
{
    global $connect;
    
    $sql = "SELECT count(reponse) as reponse FROM log_recherche where Month(date)=" . $mois . " and reponse = " . $type . " and Year(date)=" . $annee . ";";
    $res = $connect->query($sql);
    $var = $res->fetch();
    return $var['reponse'];
}

// Fonction comptant le nombre total d'utilisateur different ayant effectué des recherche sur le mois $mois
function staudif($mois, $annee)
{
    global $connect;
    
    $sql = "SELECT count(distinct(login)) as reponse FROM log_membre where Month(date)=" . $mois . " and Year(date)=" . $annee . ";";
    $res = $connect->query($sql);
    $var = $res->fetch();
    return $var['reponse'];
}

// Fonction comptant le nombre total d'utilisateur ayant effectué des recherche sur le mois $mois
function status($mois, $annee)
{
    global $connect;
    
    $sql = "SELECT count(*) as reponse FROM log_membre where Month(date)=" . $mois . " and Year(date)=" . $annee . ";";
    $res = $connect->query($sql);
    $var = $res->fetch();
    return $var['reponse'];
}

/**
 * Génère le courrier PDF, soit dans un fichier (pour être joint à un email), soit directement sur le flux de la  
 * la réponse HTTP quand il faut afficher le PDF dans l'historique
 * @param $num_rech identifiant de la recherche stocké dans la table log_recherche
 * @param $choix  "reg_pas_aide_recup", "reg_aide_recup" ou "reg_dossier_inconnu" : génère sur la réponse HTTP ; sinon
 *   génère dans une fichier
 * @param $corps_texte_pdf
 * @return le chemin où le fichier PDF est créé, null si aucun
 */
function generer_pdf($num_rech, $choix, $corps_texte_pdf)
{
    global $connect;
    
    require_once ('vendor/fpdf/balises.php');
    
    $logo_pdf_departement = config("logo_pdf_departement");
    $logo_service_pdf     = config("logo_service_pdf");
    $interlocuteur_pdf    = config("interlocuteur_pdf");
    $ville_service        = config("ville_service");
    $signature_pdf        = config("signature_pdf");
    $bas_page_pdf_adresse_department = config("bas_page_pdf_adresse_department");

    $requete = "Select lr.*, i.sexe, m.adresse_mail from log_recherche lr left join individus i on i.num_ind=lr.id_individus 
			left join membre m on m.login=lr.nom_etude where lr.id_recherche = " . $num_rech . "";
    $pdf_content = $connect->query($requete);
    $pdf_contenu = $pdf_content->fetch();
    $userNotaire = stripslashes($pdf_contenu['nom_etude']);
    $libelleuserNotaire = $pdf_contenu['libelle'];
    $prenomNotaire = stripslashes($pdf_contenu['prenom']);
    $nom_usageNotaire = stripslashes($pdf_contenu['nom_usage']);
    $dateNotaire = $pdf_contenu['date'];

    
    $pdf = new PDF_WriteTag();
    $pdf->SetMargins(20, 55, 20);
    
    $pdf->AddFont('verdana', '', 'verdana.php');
    $pdf->AddFont('verdana', 'B', 'verdanab.php');
    $pdf->AddFont('verdana', 'I', 'verdanai.php');
    $pdf->AddFont('wingding', '', 'wingding.php');
    $pdf->SetFont('verdana', '', 12);
    $pdf->AddPage();
    $pdf->Image($logo_pdf_departement, 73, 10, 65);
    // Feuille de style
    $pdf->SetStyle("p", "verdana", "N", 10, "0,0,0", 12);
    $pdf->SetStyle("po", "verdana", "N", 10, "0,0,0", 0);
    $pdf->SetStyle("coord", "verdana", "N", 7, "0,0,0", 0);
    $pdf->SetStyle("a", "verdana", "N", 10, "0,0,0", 0);
    $pdf->SetStyle("ref", "verdana", "B", 5.5, "0,0,0");
    $pdf->SetStyle("ref_L", "verdana", "N", 5.5, "0,0,0", 0);
    $pdf->SetStyle("obj", "verdana", "B", 10, "0,0,0");
    $pdf->SetStyle("pers", "verdana", "I", 0, "0,0,0");
    $pdf->SetStyle("place", "verdana", "", 0, "0,0,0");
    $pdf->SetStyle("maj", "verdana", "N", 12, "0,0,0");
    $pdf->SetStyle("pmaj", "verdana", "N", 9.5, "0,0,0");
    $pdf->SetStyle("symbole", "wingding", "N", 10, "0,0,0");
    
    $pdf->Image($logo_service_pdf, 12, 47, 82);
    $coord = utf8_decode($interlocuteur_pdf);
    $pdf->SetX(5);
    $pdf->WriteTag(150, 3, $coord, 0, "L", 0, 7);
    
    $pdf->SetX(5);
    $refs = utf8_decode("<ref>Vos ref : </ref><ref_L>  " . $userNotaire . " " . $libelleuserNotaire . "/Succession " . $prenomNotaire . " " . $nom_usageNotaire . "</ref_L>");
    $pdf->WriteTag(150, 2, $refs, 0, "L", 0, 7);
    
    $pdf->SetX(5);
    $obj = utf8_decode("<obj>Votre demande d'information sur une éventuelle créance du Département</obj>");
    $pdf->WriteTag(190, 5, $obj, 5, "L", 0, 7);
    
    /* Fix #31 Le libellé du mois avec accent est mal encodé dans le PDF
    $date_courrierNotaire = strftime("%A %d %B %Y ", strtotime($dateNotaire));
    */
    $timestamp = strtotime($dateNotaire);
    $d = date("w/j/n/Y",$timestamp);
    $date = explode("/",$d);
    $date_courrierNotaire = jourEnClair($date[0])." ".$date[1]." ".moisEnClair($date[2])." ".$date[3];
    /* */
    
    $pdf->Ln(5);
    $pdf->SetX(110);
    $txt = "<po></po><po>" . utf8_decode($ville_service) . ", le " . utf8_decode($date_courrierNotaire) . "</po>";
    $pdf->WriteTag(0, 5, $txt, 0, "R", 0, "0,0,0,7");
    
    $pdf->Ln(3);
    // Corps de texte
    $txt = utf8_decode(str_replace("&#039;","'",$corps_texte_pdf));
    
    $pdf->WriteTag(0, 5, $txt, 0, "J", 0, 7);
    
    $pdf->Ln(5);
    
    // aide_recup dossier_inconnu pas_aide_recup
    // reg_pas_aide_recup reg_aide_recup reg_dossier_inconnu
    
    // Signature
    // aide_recup reg_aide_recup
    if ($choix == "aide_recup" || $choix == "reg_aide_recup") {
        $txt1 = utf8_decode("<p>Pour le président et par délégation</p>");
        $pdf->Image($signature_pdf, 150, 240, 35);
        $pdf->WriteTag(0, 5, $txt1, 0, "R", 0, "0,0,0,7");
    } else {
        $pdf->Image($signature_pdf, 150, 230, 35);
    }
    
    $pdf->Image($bas_page_pdf_adresse_department, 55, 273, 100);
    
    $output_path = null;
    if ($choix == "reg_pas_aide_recup" || $choix == "reg_aide_recup" || $choix == "reg_dossier_inconnu") {
      // Ecriture sur la réponse HTTP directement
      ob_end_clean(); // supprime le flux HTTP déjà généré, sinon erreur
      $pdf->Output($num_rech . ".pdf", "D"); // download
    } else {
      // Ecriture dans un fichier pour joindre à l'email
      $output_path = 'pdf/' . $num_rech . '_' . $prenomNotaire . '.' . $nom_usageNotaire . '.pdf';
      $pdf->Output($output_path, 'F');
    }
    return $output_path;
}

/**
 * Envoie un email contenant la réponse à une recherche du notaire
 * 
 * @param string $mail_dest
 * @param string $mail_info
 * @param string $mail_sujet
 * @param string $mail_corps
 * @param string $lien_fichier_pdf optionnel
 */
function sendResponseEmail($mail_dest, $mail_info, $mail_sujet, $mail_corps, $lien_fichier_pdf)
{
  global $mail;
  
  // Il faut supprimer les précidents destinataires, car en cas d'appel successifs avec erreur d'envoi, les adresses
  // se cumulent
  $mail->ClearAllRecipients();
  
  $mail->AddAddress($mail_dest);
  if (isset($mail_info)) $mail->AddAddress($mail_info);
  
  $mail->Subject = $mail_sujet;
  $mail->Body = $mail_corps;
  
  if ($lien_fichier_pdf != null) {
    $mail->AddAttachment($lien_fichier_pdf);
  }
  
  if (doSendMail()) {
    $mail->Send();
  }
}

/**
 * 
 * @param string $param
 * @return string la chaîne avec les quotes (') au début et à la fin, et les caractères sépciaux de $param remplacés 
 */
function sql_escape($param)
{
  global $connect;
  return $connect->quote($param);
}

/**
 * Renvoie le titre de la page en fonction des paramètres GET
 * @param String $nom_application nom de l'application
 * @return Le titre de la page à utiliser
 */
function getPageTitle($nom_application) 
{
  $title = $nom_application;
  
  if (isset($_GET["admin"])) {
    if ($_GET["admin"] == "accueil") {
      $title = "$nom_application - Administration";
    } else {
      $title = "$nom_application - Listes des membres";
    }
  }
  else if (isset($_GET["metier"])) {
    if ($_GET["metier"] == "listing") {
      $title = "$nom_application - Listes des recherches";
    } else {
      $title = "$nom_application - Statistiques";
    }
  } else if (isset($_GET["notaire"])) {
    if ($_GET["notaire"] == "recherche") {
      $title = "$nom_application - Recherche";
    } else if ($_GET["notaire"] == "inconnu" || $_GET["notaire"] == "connu" || $_GET["notaire"] == "oups") {
      $title = "$nom_application - Réponse";
    } else {
      $title = "$nom_application - Mes précédentes recherches";
    }
  } else if (isset($_GET["index"])) {
    if ($_GET["index"] == "bis") {
      $title = "$nom_application - Modification du mot de passe";
    }
  } else {
    $title = "$nom_application - Accueil";
  }
  return $title;
}

/**
 * @param string  $name nom du paramètre GET
 * @return valeur du paramètre GET s'il est défini, null sinon
 */
function getParam($name)
{
  if ( isset($_GET[$name]) ) return $_GET[$name];
  return null;
}

/**
 * @param string  $name nom du paramètre POST
 * @return valeur du paramètre POST s'il est défini, null sinon
 */
function getPost($name)
{
  if ( isset($_POST[$name]) ) return $_POST[$name];
  return null;
}

/**
 * @param string  $name nom de la variable stockée en SESSION
 * @return valeur de la variable de SESSION si elle est définie, null sinon
 */
function getSession($name)
{
  if ( isset($_SESSION) && isset($_SESSION[$name]) ) return $_SESSION[$name];
  return null;
}

function isAdmin() {
  return profileNotaire()==PROFILE_ADMIN;
}

function isGestionnaire() {
  return profileNotaire()==PROFILE_GESTION;
}

function isNotaire() {
  return profileNotaire()==PROFILE_NOTAIRE;
}

function isLoggedIn() {
  return (isAdmin() || isGestionnaire() || isNotaire()) ? true : false;
}

/**
 *
 * @return int le code profil de l'utilisateur, 0 sinon défini
 */
function profileNotaire() {
  // Si session périmée,
  if ( empty(getSession('loginNotaire')) ) return 0;
  
  $ret = getSession('profileNotaire');
  if ( $ret==null ) return 0;
  return $ret;
}

/**
 *
 * @return string le libellé du profile de l'utilisateur
 */
function profileLabel() {
  global $PROFILE_LABELS;
  return $PROFILE_LABELS[profileNotaire()];
}

/**
 * @return true si le message de maintenance est défini
 */
function enMaintenance() {
  global $message_maintenance;
  return isset($message_maintenance) && $message_maintenance!="";
}

/**
 * Crypte le mot de passe
 * @param string $pass
 * @return la chaîne cryptéee
 */
function _passCrypt($pass)
{  // TODO : saler !
  return md5($pass);
}

/**
 * @param $user_id si non passé, utilise celui de la session
 * @return le mot de passe encodé de l'utilisateur connecté courant, null si aucun utilisateur connecté
 */
function _getUserPass($user_id=null) 
{
  global $connect;
  
  if (empty($user_id) ) {
    $user_id = getSession('id_membre');
  }
  $result = $connect->query("SELECT pass_md5 FROM membre WHERE id=".sql_escape($user_id));
  $var = $result->fetch();
  if ($var) {
    return $var["pass_md5"];
  }
  return null;
}

/**
 * Vérifie la modification du mot de passe.
 * @param int $user_id id du membren si null, utilise celui de la session éventuellement
 * @param string $pass nouveau mot de passe
 * @param string $pass_confirm confirmation du mot de passe
 * @param bool $cgu_accept true si les CGU ont été acceptées, false inon
 * @return array (err_code, err_mess)  
 *  err_code : "ERR_MANQUE_MDP" (mot de passe ou confirmation manquante), "ERR_REGLE", "ERR_CONFIRM_MDP, 
 * "ERR_CGU", "ERR_MEME_MDP", "OK"
 */
function passCheckNew($user_id, $pass, $pass_confirm, $cgu_accept)
{
  // Mot de passe non saisi, ne jamais faire confiance au contrôle JS
  if ( !$pass || !$pass_confirm ) {
    return ["ERR_MANQUE_MDP", "Vous devez fournir le mot de passe et sa confirmation"];
  }
  // Sinon Si le mot de passe ne respecte pas les règles de saisie.
  if (! preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$#", $pass)) {
    return ["ERR_REGLE",  "Le mot de passe que vous avez saisi ne respecte pas les règles"];
  }
  // Sinon Si le mot de passe de confirmation est différent du mot de passe.
  if ( $pass != $pass_confirm ) {
    return ["ERR_CONFIRM_MDP", "Le mot de passe que vous avez saisi est différent du mot de passe de confirmation"];
  }
  // CGU non accepté
  if ( !$cgu_accept ) {
    return ["ERR_CGU", "Vous devez accepter les Conditions Générales d'Utilisation"];
  }

  // Si le mot de passe est identique au mot de passe existant associé au compte.
  if (_getUserPass($user_id) == _passCrypt($pass)) {    return ["ERR_MEME_MDP", "Attention le mot de passe que vous venez de taper est identique au mot de passe existant ou qui vous a été envoyé initialement"];
  }
  
  return ["OK",""];
}

/**
 * Met à jour le mot de passe de l'utilisateur courant, et son statut de première connexion
 * @param string $new_pass
 * @param string $user_id si null utilise celui de la session
 * @return true si succès, false sinon
 */
function _passUpdate($new_pass, $user_id)
{
  global $connect;
  if (empty($user_id)) $user_id = getSession("id_membre");
  
  $req = "UPDATE membre SET pass_md5=" . sql_escape(_passCrypt($new_pass)) 
      . ", premiere_connexion=1 WHERE id=$user_id";
  $result = $connect->query($req);
  logDebug("_passUpdate : " . $req, 3);
  return ($result != false);
}

/**
 * Ecrit la valeur si $notaire_debug=true
 * @param $value valeur à écrire
 */
function echoTestValue($value)
{
  global $notaire_debug;
  if ($notaire_debug==true) echo $value;
  
}

function hasDebug($level=0) 
{
  global $notaire_debug;
  if ($notaire_debug<=0) return false;
  if ($notaire_debug>=$level) return true;
  return false;
}

function logDebug($txt, $level=1)
{
  // TODO tracer dans fichier log
  if ( hasDebug($level) ) {
    echo "<p class='debug'>" . $txt . "</p>";
  }
}

/**
 * @return true si la config 'send_mail' est true
 */
function doSendMail()
{
  return config("send_mail")==true;
}

$TEMPLARE_ENGINE = null;

/**
 * @param $template nom du fichier template se trouvant dans le dossier 'tpl'
 * @param $paramArray tableaux des paramètres. Si non défini, un tableau vide est utilisé 
 *  Les clés suivantes sont réservées : 'session', 'config'
 * @return string
 */
function templateRender($template, $paramArray=null)
{
  global $TEMPLARE_ENGINE;

  if ( !isset($paramArray) ) {
    $paramArray = [];
  }
  // Ajoute automatiquement la session
  if (isset($_SESSION) ) {
    $paramArray["session"] = $_SESSION;
  }
  
  // Ajoute automatiquement la config
  $paramArray["config"] = configAll();
  
  if (!$TEMPLARE_ENGINE) {
    $loader = new Twig_Loader_Filesystem('tpl');
    $TEMPLARE_ENGINE = new Twig_Environment($loader);
  }
  return $TEMPLARE_ENGINE->render($template, $paramArray);
}

/**
 *
 * @param array $arr
 * @param string $key
 * @return NULL si paramètre ou clé non défini
 */
function getValue($arr, $key)
{
	if (isset($arr) && isset($key) && isset($arr[$key])) {
		return $arr[$key];
	}
	return NULL;
}
/**
 * Renvoie le libellé en minuscule du jour de la semaine
 * @param int $j 0 pour Dimanche, à 6 pour Samedi 
 * @return string vide si $j n'est pas valide
 */
function jourEnClair($j)
{
    $jour = array("dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi");
    if ($j>=0 && $j<7)
    {
        return $jour[$j];
    }
    return "";
}

/**
 * Renvoie le libellé en minuscule du mois
 * @param int $m 1 pour Janvier, à 12 pour Décembre
 * @return string vide si $m n'est pas valide
 */
function moisEnClair($m)
{
    $mois = array("","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre");
    if ($m>=1 && $m<=12)
    {
        return $mois[$m];
    }
    return "";
}


?>
