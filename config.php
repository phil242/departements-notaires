﻿<?php
require_once('vendor/mail/class.phpmailer.php');
require_once("vendor/mail/class.smtp.php");

//Définit le décalage horaire par défaut de toutes les fonctions date/heure
date_default_timezone_set( 'Europe/Paris' );

// Active les traces HTML
// 0:aucune, 1:minimale, 2:tout
$notaire_debug = 0;

// Si non vide, affiche le message dans la page d'accueil
//$message_maintenance = "L'application est en maintenance jusqu'au 30 juin";


setlocale(LC_TIME, 'fr', 'fr_FR');

// Configuration BDD mysql
$host_mysql   = "localhost";
$user_mysql   = "notaires";
$pass_mysql   = "notaires";
$bdd_mysql    = "notaires";
$bdd_charset  = "UTF8";
$bdd_option   = array(PDO::MYSQL_ATTR_LOCAL_INFILE => true);

//Variables générales
$nom_application      = "Départements et Notaires";
$adresse_url_appli    = "http://localhost/notaires";
$logo_appli           = "css/images/logo.png";
$site_favicon         = "css/images/favicon.ico";
$nom_departement      = "Rhône";
$nom_departement_long = "Département du Rhône";// Nom propre du Département
$nom_departement_commun = "département du Rhône"; // Nom commun du département
$site_departement     = "https://www.rhone.fr/";
$site_cil_departement = "www.cil.rhone.fr";

// Mail
$smtp_mail      = "smtp.rhone.fr";
$port_smtp_mail = "25";
$mail_gestion  = "change.me@rhone.fr"; // Adresse générique des gestionnaires métier de l'application
$password_mail = "change me";

// Indique le mode prod, les emails sont réellement envoyés
$prod = 1;
// Message de service utilisé dans le sujet des emails
$mess_prod  = "";

// Signature des emails envoyés aux notaires
$mail_signature = "Sophie Albert,<br/>Chef de Service<br/>Service Prestations PCH, ADPA et Aide sociale<br/>Direction de l'autonomie<br/>Département du Rhône";

// PDF
$ville_service          = "Lyon"; // Ville dans l'entête de la lettre de réponse au notaire
$logo_pdf_departement   = ".private/logo_pdf.png";
$logo_pdf_departement2  = ".private/logo_.png";
$logo_service_pdf       = ".private/service_.png";
$interlocuteur_pdf      = "<coord>Votre interlocuteur  : Bureau suivi des dispositifs / Service Prestations / Direction de 
l'Autonomie</coord><symbole> )      </symbole><coord>04 72 69 69 69</coord><symbole> *      </symbole><coord>".$mail_gestion."</coord>";

$signature_pdf                   = ".private/signature.png";
$bas_page_pdf_adresse_department = ".private/baspage_.png";


// Workaround config
$NOTAIRE_CONFIG = [];

// Gestion compte
configSet("max_login", 3); // nombre max de tentatives avant blocage du compte
// Email
configSet("send_mail", true); // envoyer les emails en réel ou non


// INIT MAILER
$mail = new PHPMailer(); 
$mail->IsSMTP(); 
$mail->Host       = $smtp_mail; // SMTP server
$mail->SMTPDebug  = 0;
$mail->SMTPAuth   = true;
$mail->Port       = $port_smtp_mail;
$mail->Username   = $mail_gestion;
$mail->Password   = $password_mail; 
$mail->SetFrom($mail_gestion);
$mail->CharSet    = "UTF-8";
$mail->IsHTML(true);
// Sous Windows php 5.6 pour pouvoir envoyer les email sans erreur SSL
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);

// si présente, chargement de la config locale
if (file_exists('config_local.php')) {
	include('config_local.php');
}


// #####################################################################################################################
//
// Section non configurable
//
// #####################################################################################################################
$version_application  = "Version 1.4.1";

configSet("nom_departement", $nom_departement);
configSet("nom_departement_long", $nom_departement_long);// Nom propre du Département
configSet("nom_departement_commun", $nom_departement_commun); // Nom commun du département
configSet("site_departement", $site_departement);
configSet("nom_application", $nom_application);
configSet("mess_prod", $mess_prod);
configSet("mail_signature", $mail_signature);
configSet("site_cil_departement", $site_cil_departement);


configSet("logo_pdf_departement", $logo_pdf_departement);
configSet("logo_service_pdf", $logo_service_pdf);
configSet("interlocuteur_pdf", $interlocuteur_pdf);
configSet("ville_service", $ville_service);
configSet("signature_pdf", $signature_pdf);
configSet("bas_page_pdf_adresse_department", $bas_page_pdf_adresse_department);

configSet("adresse_url_appli", $adresse_url_appli);

// Email
configSet("mail_gestion", $mail_gestion);


// INIT CONNECT SQL
try {

  $connect = new PDO("mysql:host=$host_mysql;dbname=$bdd_mysql;charset=$bdd_charset", $user_mysql, $pass_mysql,$bdd_option);
  if (!$connect) {
    die('Invalid connect: ' . $connect->errorInfo());
  }
} catch ( PDOException $e ) {
  echo ($e->getMessage());
}


function configSet($key, $value)
{
  global $NOTAIRE_CONFIG;
  if(isset($key) && isset($value) ) $NOTAIRE_CONFIG[$key] = $value;
}
function config($key)
{
  global $NOTAIRE_CONFIG;
  if(isset($key) && isset($NOTAIRE_CONFIG[$key]) ) return $NOTAIRE_CONFIG[$key];
  return NULL;
}
// Renvoie tout le tableau des paramètres
function configAll() 
{
  global $NOTAIRE_CONFIG;
  return $NOTAIRE_CONFIG;
}


?>
