{# Corps lettre réponse : personne connue, indus probable #}


<p>Maître,</p>

<p>En réponse à votre demande concernant la succession de {{sexe}} {{prenom}} {{nom}} {{naissance}} le 
{{date_naissance}}, je vous informe qu'à la suite des recherches effectuées par mon service, la personne précitée n'a, 
à ma connaissance, jamais bénéficié d'une aide récupérable du {{config.nom_departement_long}}.
</p> 

<p>Je vous invite toutefois à questionner vos clients à cet égard.</p>

<p>Je vous signale néanmoins que cette personne a bénéficié de prestations sociales versées par ma collectivité 
(Prestation de compensation du handicap (PCH), Allocation départementale personnalisée d'autonomie (ADPA)).</p>

<p> Ces prestations ne sont pas soumises à récupération. </p> 

<p>Cependant, elles pourraient avoir fait l'objet d'un versement indu. Si tel était le cas, le Département procèdera au 
recouvrement des sommes dues auprès de votre étude. Aussi je vous invite à prendre contact avec 
la maison départementale de {{maison_departementale}} ({{tel_maison_departementale}}), en charge du dossier de 
{{sexe}} {{prenom}} {{nom}}. </p>

<p> Je vous prie d'agréer, Maître, mes courtoises salutations.</p>
