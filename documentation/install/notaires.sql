SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `notaires`
--

CREATE DATABASE IF NOT EXISTS `notaires` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `notaires`;

-- --------------------------------------------------------

--
-- Structure de la table `demande`
--

DROP TABLE IF EXISTS `demande`;
CREATE TABLE `demande` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `num_ind` int(15) NOT NULL,
  `sexe` varchar(1) NOT NULL,
  `nom_usage` varchar(250) NOT NULL,
  `nom_usage_sans` varchar(250) NOT NULL,
  `nom_civil` varchar(250) DEFAULT NULL,
  `prenom` varchar(250) NOT NULL,
  `prenom_sans` varchar(250) NOT NULL,
  `annee_naissance` int(4) NOT NULL,
  `mois_naissance` int(2) DEFAULT NULL,
  `jour_naissance` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `num_ind` (`num_ind`),
  KEY `num_ind_2` (`num_ind`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `individus`
--

DROP TABLE IF EXISTS `individus`;
CREATE TABLE `individus` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `num_ind` int(15) NOT NULL,
  `sexe` varchar(1) NOT NULL,
  `nom_usage` varchar(250) NOT NULL,
  `nom_usage_sans` varchar(250) NOT NULL,
  `nom_civil` varchar(250) DEFAULT NULL,
  `prenom` varchar(250) NOT NULL,
  `prenomd` varchar(250) DEFAULT NULL,
  `prenomt` varchar(250) DEFAULT NULL,
  `annee_naissance` int(4) NOT NULL,
  `mois_naissance` int(2) DEFAULT NULL,
  `jour_naissance` int(2) DEFAULT NULL,
  `adresse` varchar(250) NOT NULL,
  `mdr` varchar(250) NOT NULL,
  `telephone` varchar(250) NOT NULL,
  `mail_mdr` varchar(250) DEFAULT NULL,
  `libelle` varchar(250) NOT NULL,
  `code` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `num_ind` (`num_ind`),
  KEY `num_ind_2` (`num_ind`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `log_membre`
--

DROP TABLE IF EXISTS `log_membre`;
CREATE TABLE `log_membre` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `id_membre` int(12) NOT NULL,
  `login` text NOT NULL,
  `libelle` varchar(250) DEFAULT NULL,
  `adresse_mail` varchar(250) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `log_recherche`
--

DROP TABLE IF EXISTS `log_recherche`;
CREATE TABLE `log_recherche` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `id_recherche` int(15) NOT NULL,
  `nom_etude` varchar(250) NOT NULL,
  `libelle` varchar(250) DEFAULT NULL,
  `nom_usage` varchar(250) NOT NULL,
  `nom_civil` varchar(250) DEFAULT NULL,
  `prenom` varchar(250) NOT NULL,
  `date_naissance` date NOT NULL,
  `lien_pdf` varchar(250) NOT NULL,
  `date` datetime NOT NULL,
  `reponse` int(1) NOT NULL,
  `date_deces` date NOT NULL,
  `lieu_deces` varchar(250) NOT NULL,
  `date_acte` date NOT NULL,
  `id_individus` int(15) DEFAULT NULL,
  `mdr` varchar(250) DEFAULT NULL,
  `tel_mdr` varchar(250) DEFAULT NULL,
  `prenom2` varchar(255) DEFAULT NULL,
  `dest_etude` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `membre`
--

DROP TABLE IF EXISTS `membre`;
CREATE TABLE `membre` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `login` varchar(250) NOT NULL,
  `libelle` varchar(250) NOT NULL,
  `pass_md5` varchar(250) NOT NULL,
  `profile` int(2) NOT NULL,
  `premiere_connexion` int(1) NOT NULL DEFAULT '0',
  `adresse_mail` varchar(250) NOT NULL,
  `echec` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `adresse_mail` (`adresse_mail`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Contenu de la table `membre`
--

INSERT INTO `membre` (`id`, `login`, `libelle`, `pass_md5`, `profile`, `premiere_connexion`, `adresse_mail`, `echec`) VALUES
(1, 'admin', 'Compte Admin', '5ef0063a71abb4fa36fdeed3cd752a96', 3, 1, 'admin@xxx.fr', 0);

-- --------------------------------------------------------

--
-- Structure de la table `reinit_mdp`
--

DROP TABLE IF EXISTS `reinit_mdp`;
CREATE TABLE `reinit_mdp` (
  `id` int(15) NOT NULL  AUTO_INCREMENT,
  `id_membre` int(15) NOT NULL,
  `adresse_mail` varchar(250) NOT NULL,
  `tkn_cd` int(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
