![Logo](documentation/img/logo.jpg)

# Départements et Notaires

**Départements et Notaires** est un extranet permettant d'apporter une réponse en temps réel aux études notariales 
chargées d’une succession s'interrogeant sur l'existence éventuelle d'une créance du Département au titre de l'aide sociale.

**Départements et Notaires** a été initialement conçu et développé par le Département du Rhône en lien avec la Chambre 
des Notaires du Rhône, et déployé la première fois en janvier 2016 sous le nom **Rhône + Notaires**.


## Fonctionnalités

* **Pour les notaires** :
  * Recherche et réponse immédiate, à l'écran et confirmation par email avec lettre jointe PDF
  * Historique de leurs propres recherches
* **Pour les gestionnaires** :
  * Historiques de toutes les recherches réalisées
  * Statistiques


## Installation et Documentation


* [Installation](documentation/installation.md)
* [Personnalisation de l'application](documentation/config.md)
* [Documentation fonctionnelle](documentation/fonctionnalites.md)
* [Convention Département - Chambre des notaires] (https://gitlab.adullact.net/departements-notaires/departements-notaires/tree/master/documentation/Convention%20D%C3%A9partement%20-%20Chambre%20des%20notaires)



## Contacts

* Email : dev.notaires@rhone.fr
* [Site Web](https://www.rhone.fr/developpement_innovation/nouvelles_technologies/departements_notaires)

## Licence

[AGPL v3](LICENSE.txt) 
 

## Feuille de route

Bien qu'exploitée avec succès par le Département du Rhône depuis janvier 2016, l'application en version 1.3.0 n'était 
nullement destinée à être utilisée par une autre collectivité. 
La maintenabilité du code ou le respect des standards n'étaient donc pas des critères prioritaires par rapport au coût 
de réalisation et au gain de productivité offert en final.

Dans le cadre de la démarche d'ouverture entreprise avec l'Adullact, nous souhaitions pouvoir diffuser le plus rapidement
le code, avec les adaptations **minimales** facilitant la personnalisation. Cet objectif était visé par la version **1.4.0**.

La version **1.4.0** a pu être prise en main par d'autres conseils départementaux, ce qui a permis d'apporter quelques 
évolutions mineures intégrées dans la version **1.4.1**.

D'autres contributions ont déjà été faites, dont des ajouts significatifs, que nous ajoutons progressivement à la version 
en cours pour constituer la nouvelle version. Vous pouvez avoir une synthèse de ces évolutions dans le fichier 
[CHANGELOG](CHANGELOG.md) dans la section **Unreleased**. 

Nous envisageons de porter l'application sous Symfony, afin d'avoir un socle robuste pour apporter les 
évolutions futures. Cet objectif est visé par la version **2.0.0**.


Liste des versions :
* **Version actuelle** : 
  * **1.5** : Version stable intégrant des ajouts fonctionnels
  
* **Versions à venir** :
  * **2.0.0** : Version utilisant le framework Symfony
  
